-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: zebrowski.ddns.net    Database: biblioteka
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.6-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `czytelnicy`
--

DROP TABLE IF EXISTS `czytelnicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `czytelnicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imie` varchar(45) DEFAULT NULL,
  `nazwisko` varchar(45) DEFAULT NULL,
  `data_dolaczenia` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `czytelnicy`
--

LOCK TABLES `czytelnicy` WRITE;
/*!40000 ALTER TABLE `czytelnicy` DISABLE KEYS */;
INSERT INTO `czytelnicy` VALUES (1,'Karol','Nowak','2018-06-15'),(2,'Jan','Kowalski','2019-05-30'),(3,'Halinka','Kiepska','2019-09-01'),(6,'Geralt','Riv','2019-09-01');
/*!40000 ALTER TABLE `czytelnicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ksiazki`
--

DROP TABLE IF EXISTS `ksiazki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ksiazki` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ISBN` varchar(13) DEFAULT NULL,
  `tytul` varchar(45) DEFAULT NULL,
  `autor` varchar(45) DEFAULT NULL,
  `data_wydania` date DEFAULT NULL,
  `wydawnictwo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ksiazki`
--

LOCK TABLES `ksiazki` WRITE;
/*!40000 ALTER TABLE `ksiazki` DISABLE KEYS */;
INSERT INTO `ksiazki` VALUES (1,'9788372720979','Pan Kleks','Jan Brzechwa','2003-01-01','Oficyna Wydawnicza G&P'),(2,'9788328066373','Emigracja','Malcolm XD','2019-05-15','‎Wydawnictwo W.A.B.'),(3,'9788375780284','Wiedźmin Ostatnie Życzenie','Andrzej Sapkowski','2010-11-10','SUPERNOWA-Niezależna Oficyna Wydawnicza NOWA'),(4,'9788375780284','Wiedźmin Ostatnie Życzenie','Andrzej Sapkowski','2010-11-10','SUPERNOWA-Niezależna Oficyna Wydawnicza NOWA'),(5,'9788310114167','Alchemik','Michael Scott','2008-01-01','Nasza Księgarnia');
/*!40000 ALTER TABLE `ksiazki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wypozyczenia`
--

DROP TABLE IF EXISTS `wypozyczenia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wypozyczenia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `czytelnicy_id` int(11) DEFAULT NULL,
  `ksiazki_id` int(11) DEFAULT NULL,
  `wypozyczone_do` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_wypozyczajacy_has_ksiazki_ksiazki1_idx` (`ksiazki_id`),
  KEY `fk_wypozyczajacy_has_ksiazki_wypozyczajacy1_idx` (`czytelnicy_id`),
  CONSTRAINT `fk_wypozyczajacy_has_ksiazki_ksiazki1` FOREIGN KEY (`ksiazki_id`) REFERENCES `ksiazki` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_wypozyczajacy_has_ksiazki_wypozyczajacy1` FOREIGN KEY (`czytelnicy_id`) REFERENCES `czytelnicy` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wypozyczenia`
--

LOCK TABLES `wypozyczenia` WRITE;
/*!40000 ALTER TABLE `wypozyczenia` DISABLE KEYS */;
INSERT INTO `wypozyczenia` VALUES (1,2,3,'2019-08-26'),(2,2,1,'2019-09-15'),(3,6,2,'2019-09-09'),(4,3,5,'2019-09-09');
/*!40000 ALTER TABLE `wypozyczenia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'biblioteka'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-02  1:51:27
