package containers.properties;

import containers.entities.KsiazkiEntity;
import containers.entities.WypozyczeniaEntity;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.Date;

public class KsiazkiProperty {
    private final SimpleLongProperty id;
    private final SimpleStringProperty isbn;
    private final SimpleStringProperty tytul;
    private final SimpleStringProperty autor;
    private final SimpleStringProperty wydawnictwo;
    private final SimpleObjectProperty<Date> dataWydania;
    private final SimpleObjectProperty<Date> wypozyczoneDo;


    public KsiazkiProperty(KsiazkiEntity k, WypozyczeniaEntity w) {
        id = new SimpleLongProperty(k.getId());
        isbn = new SimpleStringProperty(k.getIsbn());
        tytul = new SimpleStringProperty(k.getTytul());
        autor = new SimpleStringProperty(k.getAutor());
        dataWydania = new SimpleObjectProperty<>(k.getDataWydania());
        wydawnictwo = new SimpleStringProperty(k.getWydawnictwo());
        wypozyczoneDo = new SimpleObjectProperty<>(w.getWypozyczoneDo());
    }

    public long getId() {
        return id.get();
    }

    public SimpleLongProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getIsbn() {
        return isbn.get();
    }

    public SimpleStringProperty isbnProperty() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn.set(isbn);
    }

    public String getTytul() {
        return tytul.get();
    }

    public SimpleStringProperty tytulProperty() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul.set(tytul);
    }

    public String getAutor() {
        return autor.get();
    }

    public SimpleStringProperty autorProperty() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor.set(autor);
    }

    public Date getDataWydania() {
        return dataWydania.get();
    }

    public SimpleObjectProperty<Date> dataWydaniaProperty() {
        return dataWydania;
    }

    public void setDataWydania(Date dataWydania) {
        this.dataWydania.set(dataWydania);
    }

    public String getWydawnictwo() {
        return wydawnictwo.get();
    }

    public SimpleStringProperty wydawnictwoProperty() {
        return wydawnictwo;
    }

    public void setWydawnictwo(String wydawnictwo) {
        this.wydawnictwo.set(wydawnictwo);
    }

    public Date getWypozyczoneDo() {
        return wypozyczoneDo.get();
    }

    public void setWypozyczoneDo(Date wypozyczoneDo) {
        this.wypozyczoneDo.set(wypozyczoneDo);
    }

    public SimpleObjectProperty<Date> wypozyczoneDoProperty() {
        return wypozyczoneDo;
    }
}
