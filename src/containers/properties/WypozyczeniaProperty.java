package containers.properties;

import containers.entities.CzytelnicyEntity;
import containers.entities.KsiazkiEntity;
import containers.entities.WypozyczeniaEntity;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.Date;

public class WypozyczeniaProperty {
    private final SimpleLongProperty id;
    private final SimpleLongProperty czytelnicyId;
    private final SimpleLongProperty ksiazkiId;
    private final SimpleStringProperty tytul;
    private final SimpleStringProperty autor;
    private final SimpleStringProperty isbn;
    private final SimpleObjectProperty<Date> wypozyczoneDo;
    private final SimpleStringProperty imie;
    private final SimpleStringProperty nazwisko;

    public WypozyczeniaProperty(WypozyczeniaEntity wypozyczeniaEntity, KsiazkiEntity ksiazkiEntity, CzytelnicyEntity czytelnicyEntity) {
        id = new SimpleLongProperty(wypozyczeniaEntity.getId());
        czytelnicyId = new SimpleLongProperty(wypozyczeniaEntity.getCzytelnicyId());
        ksiazkiId = new SimpleLongProperty(wypozyczeniaEntity.getKsiazkiId());
        tytul = new SimpleStringProperty(ksiazkiEntity.getTytul());
        autor = new SimpleStringProperty(ksiazkiEntity.getAutor());
        isbn = new SimpleStringProperty(ksiazkiEntity.getIsbn());
        wypozyczoneDo = new SimpleObjectProperty<>(wypozyczeniaEntity.getWypozyczoneDo());
        imie = new SimpleStringProperty(czytelnicyEntity.getImie());
        nazwisko = new SimpleStringProperty(czytelnicyEntity.getNazwisko());
    }

    public long getId() {
        return id.get();
    }

    public SimpleLongProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public long getCzytelnicyId() {
        return czytelnicyId.get();
    }

    public SimpleLongProperty czytelnicyIdProperty() {
        return czytelnicyId;
    }

    public void setCzytelnicyId(int czytelnicyId) {
        this.czytelnicyId.set(czytelnicyId);
    }

    public long getKsiazkiId() {
        return ksiazkiId.get();
    }

    public SimpleLongProperty ksiazkiIdProperty() {
        return ksiazkiId;
    }

    public void setKsiazkiId(int ksiazkiId) {
        this.ksiazkiId.set(ksiazkiId);
    }

    public Date getWypozyczoneDo() {
        return wypozyczoneDo.get();
    }

    public SimpleObjectProperty<Date> wypozyczoneDoProperty() {
        return wypozyczoneDo;
    }

    public void setWypozyczoneDo(Date wypozyczoneDo) {
        this.wypozyczoneDo.set(wypozyczoneDo);
    }

    public String getTytul() {
        return tytul.get();
    }

    public SimpleStringProperty tytulProperty() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul.set(tytul);
    }

    public String getAutor() {
        return autor.get();
    }

    public SimpleStringProperty autorProperty() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor.set(autor);
    }

    public String getIsbn() {
        return isbn.get();
    }

    public SimpleStringProperty isbnProperty() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn.set(isbn);
    }

    public String getImie() {
        return imie.get();
    }

    public void setImie(String imie) {
        this.imie.set(imie);
    }

    public SimpleStringProperty imieProperty() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko.get();
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko.set(nazwisko);
    }

    public SimpleStringProperty nazwiskoProperty() {
        return nazwisko;
    }
}
