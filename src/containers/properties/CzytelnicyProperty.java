package containers.properties;

import containers.entities.CzytelnicyEntity;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.Date;

public class CzytelnicyProperty {
    private final SimpleLongProperty id;
    private final SimpleStringProperty imie;
    private final SimpleStringProperty nazwisko;
    private final SimpleObjectProperty<Date> dataDolaczenia;

    public CzytelnicyProperty (CzytelnicyEntity entity) {
        id = new SimpleLongProperty(entity.getId());
        imie = new SimpleStringProperty(entity.getImie());
        nazwisko = new SimpleStringProperty(entity.getNazwisko());
        dataDolaczenia = new SimpleObjectProperty<>(entity.getDataDolaczenia());
    }

    @Override
    public String toString() {
        return id + ": " + imie + " " + nazwisko;
    }

    public long getId() {
        return id.get();
    }

    public SimpleLongProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getImie() {
        return imie.get();
    }

    public SimpleStringProperty imieProperty() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie.set(imie);
    }

    public String getNazwisko() {
        return nazwisko.get();
    }

    public SimpleStringProperty nazwiskoProperty() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko.set(nazwisko);
    }

    public Date getDataDolaczenia() {
        return dataDolaczenia.get();
    }

    public SimpleObjectProperty<Date> dataDolaczeniaProperty() {
        return dataDolaczenia;
    }

    public void setDataDolaczenia(Date dataDolaczenia) {
        this.dataDolaczenia.set(dataDolaczenia);
    }
}
