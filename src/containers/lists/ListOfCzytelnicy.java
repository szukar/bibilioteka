package containers.lists;

import containers.entities.CzytelnicyEntity;
import containers.properties.CzytelnicyProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.*;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

@XmlRootElement(name = "listOfCzytelnicy")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(CzytelnicyEntity.class)
public class ListOfCzytelnicy {
    @XmlElement(name = "CzytelnicyEntity")
    private List list;
    @XmlTransient
    private ObservableList<CzytelnicyProperty> observableList = null;

    private ListOfCzytelnicy() { }

    public ListOfCzytelnicy(List l) {
        list = l;
    }

    public List getList() {
        return list;
    }

    public ObservableList<CzytelnicyProperty> getObservableList() {
        if (observableList == null) {
            observableList = FXCollections.observableArrayList();
        }
        observableList.clear();
        if (list != null && list.size() > 0) {
            for (Object o : list) {
                observableList.add(new CzytelnicyProperty((CzytelnicyEntity) o));
            }
        }
        return observableList;
    }

    public static String toXML(ListOfCzytelnicy object) {
        StringWriter stringWriter = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(ListOfCzytelnicy.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(object, stringWriter);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return stringWriter.toString();
    }

    public static ListOfCzytelnicy toObject(String xml) {
        ListOfCzytelnicy listOfCzytelnicy = null;
        try {
            JAXBContext context = JAXBContext.newInstance(ListOfCzytelnicy.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            listOfCzytelnicy = (ListOfCzytelnicy) unmarshaller.unmarshal(new StreamSource(new StringReader(xml)));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return listOfCzytelnicy;
    }
}
