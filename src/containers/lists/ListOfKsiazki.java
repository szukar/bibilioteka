package containers.lists;

import containers.entities.KsiazkiEntity;
import containers.entities.WypozyczeniaEntity;
import containers.properties.KsiazkiProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.*;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

@XmlRootElement(name = "listOfKsiazki")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(KsiazkiEntity.class)
public class ListOfKsiazki {
    @XmlElement(name = "KsiazkiEntity", type = KsiazkiEntity.class)
    private List listK;
    @XmlElement(name = "WypozyczeniaEntity", type = WypozyczeniaEntity.class)
    private List listW;
    @XmlTransient
    private ObservableList<KsiazkiProperty> observableList = null;

    private ListOfKsiazki() { }

    public ListOfKsiazki(List k, List w) {
        listK = k;
        listW = w;
    }

    public List getListK() {
        return listK;
    }

    public ObservableList<KsiazkiProperty> getObservableList() {
        if (observableList == null) {
            observableList = FXCollections.observableArrayList();
        }
        observableList.clear();
        if (listK != null && listK.size() > 0) {
            boolean added;
            for (Object ksiazki : listK) {
                KsiazkiEntity k = (KsiazkiEntity) ksiazki;
                added = false;
                if (listW != null && listW.size() > 0) {
                    for (Object wypozyczenia : listW) {
                        WypozyczeniaEntity w = (WypozyczeniaEntity) wypozyczenia;
                        if (k.getId().compareTo(w.getKsiazkiId()) == 0) {
                            observableList.add(new KsiazkiProperty(k, w));
                            listW.remove(w);
                            added = true;
                            break;
                        }
                    }
                }
                if (!added) {
                    observableList.add(new KsiazkiProperty(k, new WypozyczeniaEntity()));
                }
            }
        }

        return observableList;
    }

    public static String toXML(ListOfKsiazki object) {
        StringWriter stringWriter = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(ListOfKsiazki.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(object, stringWriter);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return stringWriter.toString();
    }

    public static ListOfKsiazki toObject(String xml) {
        ListOfKsiazki listOfKsiazki = null;
        try {
            JAXBContext context = JAXBContext.newInstance(ListOfKsiazki.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            listOfKsiazki = (ListOfKsiazki) unmarshaller.unmarshal(new StreamSource(new StringReader(xml)));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return listOfKsiazki;
    }
}
