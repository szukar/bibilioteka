package containers.lists;

import containers.entities.CzytelnicyEntity;
import containers.entities.KsiazkiEntity;
import containers.entities.WypozyczeniaEntity;
import containers.properties.WypozyczeniaProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.*;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

@XmlRootElement(name = "listOfWypozyczenia")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(WypozyczeniaEntity.class)
public class ListOfWypozyczenia {
    @XmlElement(name = "WypozyczeniaEntity", type = WypozyczeniaEntity.class)
    private List listW;
    @XmlElement(name = "KsiazkiEntity", type = KsiazkiEntity.class)
    private List listK;
    @XmlElement(name = "CzytelnicyEntity", type = CzytelnicyEntity.class)
    private List listC;
    @XmlTransient
    private ObservableList<WypozyczeniaProperty> observableList = null;

    private ListOfWypozyczenia() { }

    public ListOfWypozyczenia(List w, List k, List c) {
        listW = w;
        listK = k;
        listC = c;
    }

    public List getListW() {
        return listW;
    }

    public List getListK() {
        return listK;
    }

    public List getListC() {
        return listC;
    }

    public ObservableList<WypozyczeniaProperty> getObservableList() {
        if (observableList == null) {
            observableList = FXCollections.observableArrayList();
        }
        observableList.clear();
        if (listW != null && listW.size() > 0) {
            for (Object wypozyczenia : listW) {
                WypozyczeniaEntity w = (WypozyczeniaEntity) wypozyczenia;
                for (Object ksiazki : listK) {
                    KsiazkiEntity k = (KsiazkiEntity) ksiazki;
                    if (w.getKsiazkiId().equals(k.getId())) {
                        listK.remove(k);
                        for (Object czytelnicy : listC) {
                            CzytelnicyEntity c = (CzytelnicyEntity) czytelnicy;
                            if (w.getKsiazkiId().equals(k.getId())) {
                                observableList.add(new WypozyczeniaProperty((WypozyczeniaEntity) wypozyczenia, (KsiazkiEntity) ksiazki, (CzytelnicyEntity) czytelnicy));
                                listK.remove(k);
                                break;
                            }
                        }
                        break;
                    }
                }

            }
        }
        return observableList;
    }

    public static String toXML(ListOfWypozyczenia object) {
        StringWriter stringWriter = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(ListOfWypozyczenia.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(object, stringWriter);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return stringWriter.toString();
    }

    public static ListOfWypozyczenia toObject(String xml) {
        ListOfWypozyczenia listOfWypozyczenia = null;
        try {
            JAXBContext context = JAXBContext.newInstance(ListOfWypozyczenia.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            listOfWypozyczenia = (ListOfWypozyczenia) unmarshaller.unmarshal(new StreamSource(new StringReader(xml)));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return listOfWypozyczenia;
    }
}
