package containers.entities;

import containers.adapters.DateAdapter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Date;
import java.util.Objects;

/**
 * Klasa reprezentująca encję książki w bazie
 */
@XmlRootElement(name = "CzytelnicyEntity")
@XmlAccessorType(XmlAccessType.FIELD)
public class CzytelnicyEntity {
    /**
     * Prywatne pole przechowujące id
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    /**
     * Prywatne pole przechowujące imię
     */
    private String imie;
    /**
     * Prywatne pole przechowujące nazwisko
     */
    private String nazwisko;
    /**
     * Prywatne pole przechowujące datę założenia karty
     */
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date dataDolaczenia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public Date getDataDolaczenia() {
        return dataDolaczenia;
    }

    public void setDataDolaczenia(Date dataDolaczenia) {
        this.dataDolaczenia = dataDolaczenia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CzytelnicyEntity that = (CzytelnicyEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(imie, that.imie) &&
                Objects.equals(nazwisko, that.nazwisko) &&
                Objects.equals(dataDolaczenia, that.dataDolaczenia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, imie, nazwisko, dataDolaczenia);
    }

    public static String toXML(CzytelnicyEntity entity) {
        StringWriter stringWriter = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(CzytelnicyEntity.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(entity, stringWriter);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return stringWriter.toString();
    }

    public static CzytelnicyEntity toObject(String xml) {
        CzytelnicyEntity entity = null;
        try {
            JAXBContext context = JAXBContext.newInstance(CzytelnicyEntity.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            entity = (CzytelnicyEntity) unmarshaller.unmarshal(new StreamSource(new StringReader(xml)));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return entity;
    }
}
