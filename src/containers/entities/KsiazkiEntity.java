package containers.entities;

import containers.adapters.DateAdapter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Date;
import java.util.Objects;

@XmlRootElement(name = "KsiazkiEntity")
@XmlAccessorType(XmlAccessType.FIELD)
public class KsiazkiEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String isbn;
    private String tytul;
    private String autor;
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date dataWydania;
    private String wydawnictwo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public Date getDataWydania() {
        return dataWydania;
    }

    public void setDataWydania(Date dataWydania) {
        this.dataWydania = dataWydania;
    }

    public String getWydawnictwo() {
        return wydawnictwo;
    }

    public void setWydawnictwo(String wydawnictwo) {
        this.wydawnictwo = wydawnictwo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KsiazkiEntity that = (KsiazkiEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(isbn, that.isbn) &&
                Objects.equals(tytul, that.tytul) &&
                Objects.equals(autor, that.autor) &&
                Objects.equals(dataWydania, that.dataWydania) &&
                Objects.equals(wydawnictwo, that.wydawnictwo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, isbn, tytul, autor, dataWydania, wydawnictwo);
    }

    public static String toXML(KsiazkiEntity entity) {
        StringWriter stringWriter = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(KsiazkiEntity.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(entity, stringWriter);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return stringWriter.toString();
    }

    public static KsiazkiEntity toObject(String xml) {
        KsiazkiEntity entity = null;
        try {
            JAXBContext context = JAXBContext.newInstance(KsiazkiEntity.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            entity = (KsiazkiEntity) unmarshaller.unmarshal(new StreamSource(new StringReader(xml)));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return entity;
    }
}
