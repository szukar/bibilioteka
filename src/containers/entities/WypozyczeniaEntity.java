package containers.entities;

import containers.adapters.DateAdapter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Date;
import java.util.Objects;

@XmlRootElement(name = "WypozyczeniaEntity")
@XmlAccessorType(XmlAccessType.FIELD)
public class WypozyczeniaEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long czytelnicyId;
    private Long ksiazkiId;
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date wypozyczoneDo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getWypozyczoneDo() {
        return wypozyczoneDo;
    }

    public void setWypozyczoneDo(Date wypozyczoneDo) {
        this.wypozyczoneDo = wypozyczoneDo;
    }

    public Long getCzytelnicyId() {
        return czytelnicyId;
    }

    public void setCzytelnicyId(Long czytelnicyId) {
        this.czytelnicyId = czytelnicyId;
    }

    public Long getKsiazkiId() {
        return ksiazkiId;
    }

    public void setKsiazkiId(Long ksiazkiId) {
        this.ksiazkiId = ksiazkiId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WypozyczeniaEntity that = (WypozyczeniaEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(wypozyczoneDo, that.wypozyczoneDo) &&
                Objects.equals(czytelnicyId, that.czytelnicyId) &&
                Objects.equals(ksiazkiId, that.ksiazkiId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, czytelnicyId, ksiazkiId, wypozyczoneDo);
    }

    public static String toXML(WypozyczeniaEntity entity) {
        StringWriter stringWriter = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(WypozyczeniaEntity.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(entity, stringWriter);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return stringWriter.toString();
    }

    public static WypozyczeniaEntity toObject(String xml) {
        WypozyczeniaEntity entity = null;
        try {
            JAXBContext context = JAXBContext.newInstance(WypozyczeniaEntity.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            entity = (WypozyczeniaEntity) unmarshaller.unmarshal(new StreamSource(new StringReader(xml)));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return entity;
    }
}
