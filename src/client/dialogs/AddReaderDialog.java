package client.dialogs;

import client.limiters.LengthLimiter;
import containers.entities.CzytelnicyEntity;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

import java.util.Date;

/**
 * Klasa wyświetlająca dialog dodawania nowego czytelnika
 * @version 1.0
 * @author Szymon Żebrowski
 */
public class AddReaderDialog extends Dialog<Pair<String, CzytelnicyEntity>> {
    public AddReaderDialog() {
        setTitle("Nowy czytelnik");
        setHeaderText("Podaj dane nowego czytelnika");

        // Set the button types.
        ButtonType addButtonType = new ButtonType("Dodaj", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancelButtonType = new ButtonType("Anuluj", ButtonBar.ButtonData.CANCEL_CLOSE);
        getDialogPane().getButtonTypes().addAll(addButtonType, cancelButtonType);

        // Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField imie = new TextField();
        LengthLimiter.addLengthLimiter(imie, 45);
        imie.setPromptText("Imię");
        TextField nazwisko = new TextField();
        LengthLimiter.addLengthLimiter(nazwisko, 45);
        nazwisko.setPromptText("Nazwisko");

        grid.add(new Label("Imię:"), 0, 0);
        grid.add(imie, 1, 0);
        grid.add(new Label("Nazwisko:"), 0, 1);
        grid.add(nazwisko, 1, 1);

        getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(imie::requestFocus);

        // Convert the result to a username-password-pair when the login button is clicked.
        setResultConverter(dialogButton -> {
            if (dialogButton == addButtonType) {
                if (!imie.getText().trim().isEmpty() && !nazwisko.getText().trim().isEmpty()) {
                    CzytelnicyEntity entity = new CzytelnicyEntity();
                    entity.setImie(imie.getText());
                    entity.setNazwisko(nazwisko.getText());
                    entity.setDataDolaczenia(new Date());
                    return new Pair<>("success", entity);
                } else {
                    return new Pair<>("blank fields", null);
                }
            }
            return null;
        });
    }
}
