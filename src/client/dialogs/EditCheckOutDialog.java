package client.dialogs;

import containers.entities.WypozyczeniaEntity;
import containers.properties.WypozyczeniaProperty;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Klasa wyświetlająca dialog edytowania istniejącego wypożyczenia
 * @version 1.0
 * @author Szymon Żebrowski
 */
public class EditCheckOutDialog extends Dialog<Pair<String, WypozyczeniaEntity>> {
    public EditCheckOutDialog(WypozyczeniaProperty property) {
        setTitle("Istniejące wypożyczenie");
        setHeaderText("Przedłuż wypożyczenie lub je usuń.");

        // Set the button types.
        ButtonType prolongButtonType = new ButtonType("Przedłuż", ButtonBar.ButtonData.OK_DONE);
        ButtonType removeButtonType = new ButtonType("Usuń", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancelButtonType = new ButtonType("Anuluj", ButtonBar.ButtonData.CANCEL_CLOSE);
        getDialogPane().getButtonTypes().addAll(prolongButtonType, removeButtonType, cancelButtonType);

        // Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField nrKarty = new TextField();
        nrKarty.setText(String.valueOf(property.getCzytelnicyId()));
        nrKarty.setEditable(false);
        TextField imie = new TextField();
        imie.setEditable(false);
        imie.setText(property.getImie());
        TextField nazwisko = new TextField();
        nazwisko.setText(property.getNazwisko());
        nazwisko.setEditable(false);

        grid.add(new Label("Numer Karty:"), 0, 0);
        grid.add(nrKarty, 1, 0);
        grid.add(new Label("Imię:"), 0, 1);
        grid.add(imie, 1, 1);
        grid.add(new Label("Nazwisko:"), 0, 2);
        grid.add(nazwisko, 1, 2);
        grid.add(new Separator(), 0, 3);
        grid.add(new Separator(), 1, 3);

        TextField idKsiazki = new TextField();
        idKsiazki.setText(String.valueOf(property.getId()));
        idKsiazki.setEditable(false);
        TextField tytul = new TextField();
        tytul.setText(property.getTytul());
        tytul.setEditable(false);
        TextField autor = new TextField();
        autor.setText(property.getAutor());
        autor.setEditable(false);
        TextField isbn = new TextField();
        isbn.setText(property.getIsbn());
        isbn.setEditable(false);

        grid.add(new Label("Numer książki:"), 0, 4);
        grid.add(idKsiazki, 1, 4);
        grid.add(new Label("Tytuł:"), 0, 5);
        grid.add(tytul, 1, 5);
        grid.add(new Label("Autor:"), 0, 6);
        grid.add(autor, 1, 6);
        grid.add(new Label("ISBN:"), 0, 7);
        grid.add(isbn, 1, 7);
        grid.add(new Separator(), 0, 8);
        grid.add(new Separator(), 1, 8);

        TextField wypozyczoneDo = new TextField();
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        wypozyczoneDo.setText(format.format(property.getWypozyczoneDo()));
        wypozyczoneDo.setEditable(false);

        grid.add(new Label("Wypożyczone do:"), 0, 9);
        grid.add(wypozyczoneDo, 1, 9);

        getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(imie::requestFocus);

        // Convert the result to a username-password-pair when the login button is clicked.
        setResultConverter(dialogButton -> {
            if (dialogButton == prolongButtonType) {
                WypozyczeniaEntity entity = new WypozyczeniaEntity();
                entity.setId(property.getId());
                entity.setKsiazkiId(property.getKsiazkiId());
                entity.setCzytelnicyId(property.getCzytelnicyId());
                Calendar c = Calendar.getInstance();
                c.setTime(new Date());
                c.add(Calendar.MONTH, 1);
                entity.setWypozyczoneDo(c.getTime());
                return new Pair<>("prolong", entity);
            } else if (dialogButton == removeButtonType) {
                WypozyczeniaEntity entity = new WypozyczeniaEntity();
                entity.setId(property.getId());
                entity.setKsiazkiId(property.getKsiazkiId());
                entity.setCzytelnicyId(property.getCzytelnicyId());
                entity.setWypozyczoneDo(property.getWypozyczoneDo());
                return new Pair<>("remove", entity);
            }
            return null;
        });
    }
}
