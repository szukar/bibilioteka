package client.dialogs;

import client.ConnectionToServer;
import client.limiters.LengthLimiter;
import client.limiters.TypeLimiter;
import containers.entities.CzytelnicyEntity;
import containers.entities.WypozyczeniaEntity;
import containers.lists.ListOfCzytelnicy;
import containers.properties.CzytelnicyProperty;
import containers.properties.KsiazkiProperty;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;
import javafx.util.Pair;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Klasa wyświetlająca dialog dodawania nowego wypożyczenia
 * @version 1.0
 * @author Szymon Żebrowski
 */
public class AddCheckOutDialog extends Dialog<Pair<String, WypozyczeniaEntity>> {
    /**
     * Callback dzięki któremu lista czytelników jest wyświetlana w czytelny sposób
     */
    private Callback<ListView<CzytelnicyProperty>, ListCell<CzytelnicyProperty>> cellFactory = new Callback<>() {

        @Override
        public ListCell<CzytelnicyProperty> call(ListView<CzytelnicyProperty> l) {
            return new ListCell<>() {

                @Override
                protected void updateItem(CzytelnicyProperty item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setGraphic(null);
                    } else {
                        setText(item.getId() + ": " + item.getImie() + " " + item.getNazwisko());
                    }
                }
            };
        }
    };

    public AddCheckOutDialog(KsiazkiProperty property, ConnectionToServer connection) {
        setTitle("Nowe wypożyczenie");
        setHeaderText("Wybierz czytelnika");

        ButtonType addButtonType = new ButtonType("Zmień", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancelButtonType = new ButtonType("Anuluj", ButtonBar.ButtonData.CANCEL_CLOSE);
        getDialogPane().getButtonTypes().addAll(addButtonType, cancelButtonType);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField idKsiazki = new TextField();
        idKsiazki.setText(String.valueOf(property.getId()));
        idKsiazki.setEditable(false);
        TextField tytul = new TextField();
        tytul.setText(property.getTytul());
        tytul.setEditable(false);
        TextField autor = new TextField();
        autor.setText(property.getAutor());
        autor.setEditable(false);
        TextField isbn = new TextField();
        isbn.setText(property.getIsbn());
        isbn.setEditable(false);
        TextField wydawnictwo = new TextField();
        wydawnictwo.setText(property.getWydawnictwo());
        wydawnictwo.setEditable(false);
        TextField dataWydania = new TextField();
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        dataWydania.setText(format.format(property.getDataWydania()));
        dataWydania.setEditable(false);

        grid.add(new Label("Tytuł:"), 0, 0);
        grid.add(tytul, 1, 0);
        grid.add(new Label("Autor:"), 0, 1);
        grid.add(autor, 1, 1);
        grid.add(new Label("ISBN:"), 0, 2);
        grid.add(isbn, 1, 2);
        grid.add(new Label("Wydawnictwo:"), 0, 3);
        grid.add(wydawnictwo, 1, 3);
        grid.add(new Label("Data wydania:"), 0, 4);
        grid.add(dataWydania, 1, 4);
        grid.add(new Separator(), 0, 5);
        grid.add(new Separator(), 1, 5);

        TextField idCzytelnika = new TextField();
        LengthLimiter.addLengthLimiter(idCzytelnika, 11);
        TypeLimiter.addNumbersTypeLimiter(idCzytelnika);
        idCzytelnika.setPromptText("Numer karty");
        TextField imie = new TextField();
        LengthLimiter.addLengthLimiter(imie, 45);
        imie.setPromptText("Imię");
        TextField nazwisko = new TextField();
        LengthLimiter.addLengthLimiter(nazwisko, 45);
        nazwisko.setPromptText("Nazwisko");
        Button button = new Button("Szukaj");
        Label info = new Label();
        ComboBox<CzytelnicyProperty> comboBox = new ComboBox<>();
        ObservableList<CzytelnicyProperty> czytelnicyProperties = connection.listAllReaders().getObservableList();
        info.setText(czytelnicyProperties.size() + " wyników:");
        comboBox.setItems(czytelnicyProperties);
        comboBox.setButtonCell(cellFactory.call(null));
        comboBox.setCellFactory(cellFactory);
        button.setOnAction(actionEvent -> {
            CzytelnicyEntity entity = new CzytelnicyEntity();
            if (!idCzytelnika.getText().equals("")) {
                entity.setId(Long.parseLong(idCzytelnika.getText()));
            } else {
                if (!imie.getText().equals("")) {
                    entity.setImie("%" + imie.getText() + "%");
                }
                if (!nazwisko.getText().equals("")) {
                    entity.setNazwisko("%" + nazwisko.getText() + "%");
                }
            }
            ListOfCzytelnicy list;
            if (entity.equals(new CzytelnicyEntity())) {
                list = connection.listAllReaders();
            } else {
                list = connection.searchReaders(entity);
            }
            ObservableList<CzytelnicyProperty> list1 = list.getObservableList();
            info.setText(list1.size() + " wyników:");
            comboBox.setItems(list1);
        });


        grid.add(new Label("Numer Karty:"), 0, 6);
        grid.add(idCzytelnika, 1, 6);
        grid.add(new Label("Imię:"), 0, 7);
        grid.add(imie, 1, 7);
        grid.add(new Label("Nazwisko:"), 0, 8);
        grid.add(nazwisko, 1, 8);
        grid.add(new Label("Szukaj:"), 0, 9);
        grid.add(button, 1, 9);
        grid.add(info, 0, 10);
        grid.add(comboBox, 1, 10);

        getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(tytul::requestFocus);

        // Convert the result to a username-password-pair when the login button is clicked.
        setResultConverter(dialogButton -> {
            if (dialogButton == addButtonType) {
                if (comboBox.getValue() != null) {
                    WypozyczeniaEntity entity = new WypozyczeniaEntity();
                    entity.setCzytelnicyId(comboBox.getValue().getId());
                    entity.setKsiazkiId(property.getId());
                    Calendar c = Calendar.getInstance();
                    c.setTime(new Date());
                    c.add(Calendar.MONTH, 1);
                    entity.setWypozyczoneDo(c.getTime());
                    return new Pair<>("add", entity);
                } else {
                    return new Pair<>("blank fields", null);
                }
            }
            return null;
        });
    }
}
