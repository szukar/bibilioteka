package client.dialogs;

import client.limiters.LengthLimiter;
import containers.entities.KsiazkiEntity;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

import java.time.LocalDate;

/**
 * Klasa wyświetlająca dialog dodawania nowej książki
 * @version 1.0
 * @author Szymon Żebrowski
 */
public class AddBookDialog extends Dialog<Pair<String, KsiazkiEntity>> {
    public AddBookDialog() {
        setTitle("Nowa książka");
        setHeaderText("Podaj dane nowej książki");

        ButtonType addButtonType = new ButtonType("Dodaj", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancelButtonType = new ButtonType("Anuluj", ButtonBar.ButtonData.CANCEL_CLOSE);
        getDialogPane().getButtonTypes().addAll(addButtonType, cancelButtonType);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField tytul = new TextField();
        LengthLimiter.addLengthLimiter(tytul, 45);
        tytul.setPromptText("Tytuł");
        TextField autor = new TextField();
        LengthLimiter.addLengthLimiter(autor, 45);
        autor.setPromptText("Autor");
        TextField isbn = new TextField();
        LengthLimiter.addLengthLimiter(isbn, 13);
        isbn.setPromptText("ISBN");
        TextField wydawnictwo = new TextField();
        LengthLimiter.addLengthLimiter(wydawnictwo, 45);
        wydawnictwo.setPromptText("Wydawnictwo");
        DatePicker dataWydania = new DatePicker();
        dataWydania.setDayCellFactory(datePicker -> new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();
                setDisable(empty || date.compareTo(today) > 0);
            }
        });

        grid.add(new Label("Tytuł:"), 0, 0);
        grid.add(tytul, 1, 0);
        grid.add(new Label("Autor:"), 0, 1);
        grid.add(autor, 1, 1);
        grid.add(new Label("ISBN:"), 0, 2);
        grid.add(isbn, 1, 2);
        grid.add(new Label("Wydawnictwo:"), 0, 3);
        grid.add(wydawnictwo, 1, 3);
        grid.add(new Label("Data wydania:"), 0, 4);
        grid.add(dataWydania, 1, 4);

        getDialogPane().setContent(grid);

        Platform.runLater(tytul::requestFocus);

        setResultConverter(dialogButton -> {
            if (dialogButton == addButtonType) {
                if (tytul.getText().trim().isEmpty() || autor.getText().trim().isEmpty() || isbn.getText().trim().isEmpty() ||
                        wydawnictwo.getText().trim().isEmpty() || dataWydania.getValue() == null) {
                    String reason = "blank fields";
                    return new Pair<>(reason, null);
                } else if (!validateISBN(isbn.getText())) {
                    String reason = "bad isbn";
                    return new Pair<>(reason, null);
                } else {
                    KsiazkiEntity entity = new KsiazkiEntity();
                    entity.setTytul(tytul.getText());
                    entity.setAutor(autor.getText());
                    entity.setIsbn(isbn.getText());
                    entity.setWydawnictwo(wydawnictwo.getText());
                    entity.setDataWydania(java.sql.Date.valueOf(dataWydania.getValue()));
                    return new Pair<>("success", entity);
                }
            }
            return null;
        });
    }

    private boolean validateISBN(String isbn) {
        if (isbn == null) {
            return false;
        }

        //usuwanie myślników
        isbn = isbn.replaceAll("-", "");

        //musi mieć 13 znaków
        if (isbn.length() != 13) {
            return false;
        }

        try {
            int tot = 0;
            for (int i = 0; i < 12; i++) {
                int digit = Integer.parseInt(isbn.substring(i, i + 1));
                tot += (i % 2 == 0) ? digit : digit * 3;
            }

            //znak kontrolny musi być w przedziale 0-9, jeżeli wynosi 10 ustawiamy go na 0
            int checksum = 10 - (tot % 10);
            if (checksum == 10) {
                checksum = 0;
            }

            return checksum == Integer.parseInt(isbn.substring(12));
        } catch (NumberFormatException e) {
            //łapie wyjątek gdy znak nie jest liczbą
            return false;
        }
    }
}
