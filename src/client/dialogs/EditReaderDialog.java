package client.dialogs;

import client.limiters.LengthLimiter;
import containers.entities.CzytelnicyEntity;
import containers.properties.CzytelnicyProperty;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

/**
 * Klasa wyświetlająca dialog edytowania istniejącego czytelnika
 * @version 1.0
 * @author Szymon Żebrowski
 */
public class EditReaderDialog extends Dialog<Pair<String, CzytelnicyEntity>> {
    public EditReaderDialog (CzytelnicyProperty property) {
        setTitle("Istniejący czytelnik");
        setHeaderText("Edytuj dane czytelnika lub go usuń.");

        // Set the button types.
        ButtonType editButtonType = new ButtonType("Zmień", ButtonBar.ButtonData.OK_DONE);
        ButtonType removeButtonType = new ButtonType("Usuń", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancelButtonType = new ButtonType("Anuluj", ButtonBar.ButtonData.CANCEL_CLOSE);
        getDialogPane().getButtonTypes().addAll(editButtonType, removeButtonType, cancelButtonType);

        // Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField imie = new TextField();
        LengthLimiter.addLengthLimiter(imie, 45);
        imie.setText(property.getImie());
        imie.setPromptText("Imię");
        TextField nazwisko = new TextField();
        LengthLimiter.addLengthLimiter(nazwisko, 45);
        nazwisko.setText(property.getNazwisko());
        nazwisko.setPromptText("Nazwisko");

        grid.add(new Label("Imię:"), 0, 0);
        grid.add(imie, 1, 0);
        grid.add(new Label("Nazwisko:"), 0, 1);
        grid.add(nazwisko, 1, 1);

        getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(imie::requestFocus);

        // Convert the result to a username-password-pair when the login button is clicked.
        setResultConverter(dialogButton -> {
            if (dialogButton == editButtonType) {
                if (!imie.getText().trim().isEmpty() && !nazwisko.getText().trim().isEmpty()) {
                    CzytelnicyEntity entity = new CzytelnicyEntity();
                    entity.setId(property.getId());
                    entity.setImie(imie.getText());
                    entity.setNazwisko(nazwisko.getText());
                    entity.setDataDolaczenia(property.getDataDolaczenia());
                    return new Pair<>("edit", entity);
                } else {
                    String reason = "blank fields";
                    return new Pair<>(reason, null);
                }
            } else if (dialogButton == removeButtonType) {
                CzytelnicyEntity entity = new CzytelnicyEntity();
                entity.setId(property.getId());
                entity.setImie(property.getImie());
                entity.setNazwisko(property.getNazwisko());
                entity.setDataDolaczenia(property.getDataDolaczenia());
                return new Pair<>("remove", entity);
            }
            return null;
        });
    }
}
