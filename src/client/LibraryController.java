package client;

import client.dialogs.*;
import client.limiters.TypeLimiter;
import containers.entities.CzytelnicyEntity;
import containers.entities.KsiazkiEntity;
import containers.entities.WypozyczeniaEntity;
import containers.lists.ListOfCzytelnicy;
import containers.lists.ListOfKsiazki;
import containers.lists.ListOfWypozyczenia;
import containers.properties.CzytelnicyProperty;
import containers.properties.KsiazkiProperty;
import containers.properties.WypozyczeniaProperty;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.util.Pair;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * Kontroler głównego okna
 * @author Szymon Żebrowski
 * @version 1.0
 */
public class LibraryController {
    /**
     * Prywatne pole przechowujące obiekt reprezentujący połączenie z serwerem
     */
    private final ConnectionToServer connection = new ConnectionToServer();

    /**
     * Prywatne pole przechowujące referencję do pola tekstowego wyszukiwarki z id książki
     */
    @FXML
    private TextField ksiazkiIdSearchBox;
    /**
     * Prywatne pole przechowujące referencję do pola tekstowego wyszukiwarki z tytułem książki
     */
    @FXML
    private TextField ksiazkiTytulSearchBox;
    /**
     * Prywatne pole przechowujące referencję do pola tekstowego wyszukiwarki z autorem książki
     */
    @FXML
    private TextField ksiazkiAutorSearchBox;
    /**
     * Prywatne pole przechowujące referencję do pola tekstowego wyszukiwarki z nr ISBN książki
     */
    @FXML
    private TextField ksiazkiIsbnSearchBox;
    /**
     * Prywatne pole przechowujące referencję do pola tekstowego wyszukiwarki z wydawnictwem książki
     */
    @FXML
    private TextField ksiazkiWydawnictwoSearchBox;
    /**
     * Prywatne pole przechowujące referencję tabeli z książkami
     */
    @FXML
    private TableView<KsiazkiProperty> ksiazkiTable;
    /**
     * Prywatne pole przechowujące referencję kolumny z id książki
     */
    @FXML
    private TableColumn<KsiazkiProperty, Long> ksiazkiIdColumn;
    /**
     * Prywatne pole przechowujące referencję kolumny z tytułem książki
     */
    @FXML
    private TableColumn<KsiazkiProperty, String> ksiazkiTytulColumn;
    /**
     * Prywatne pole przechowujące referencję kolumny z autorem książki
     */
    @FXML
    private TableColumn<KsiazkiProperty, String> ksiazkiAutorColumn;
    /**
     * Prywatne pole przechowujące referencję kolumny z nr ISBN książki
     */
    @FXML
    private TableColumn<KsiazkiProperty, String> ksiazkiIsbnColumn;
    /**
     * Prywatne pole przechowujące referencję kolumny z wydawnictwem książki
     */
    @FXML
    private TableColumn<KsiazkiProperty, String> ksiazkiWydawnictwoColumn;
    /**
     * Prywatne pole przechowujące referencję kolumny z datą wydania książki
     */
    @FXML
    private TableColumn<KsiazkiProperty, Date> ksiazkiDataWydaniaColumn;
    /**
     * Prywatne pole przechowujące referencję kolumny z datą do której książka jest wypożyczona, w kolumnie tej widnieje
     * napis "Dostępna", gdy książka nie jest wypożyczona
     */
    @FXML
    private TableColumn<KsiazkiProperty, Date> ksiazkiWypozyczoneDoColumn;

    /**
     * Prywatne pole przechowujące referencję do pola tekstowego wyszukiwarki z id czytelnika
     */
    @FXML
    private TextField wypozyczeniaKartySearchBox;
    /**
     * Prywatne pole przechowujące referencję do pola tekstowego wyszukiwarki z id książki
     */
    @FXML
    private TextField wypozyczeniaKsiazkiSearchBox;
    /**
     * Prywatne pole przechowujące referencję tabeli z wypożyczeniami
     */
    @FXML
    private TableView<WypozyczeniaProperty> wypozyczeniaTable;
    /**
     * Prywatne pole przechowujące referencję kolumny z id czytelnika
     */
    @FXML
    private TableColumn<WypozyczeniaProperty, Long> wypozyczeniaIdKartyColumn;
    /**
     * Prywatne pole przechowujące referencję kolumny z id książki
     */
    @FXML
    private TableColumn<WypozyczeniaProperty, Long> wypozyczeniaIdKsiazkiColumn;
    /**
     * Prywatne pole przechowujące referencję kolumny z tytułem książki
     */
    @FXML
    private TableColumn<WypozyczeniaProperty, String> wypozyczeniaTytulColumn;
    /**
     * Prywatne pole przechowujące referencję kolumny z autorem książki
     */
    @FXML
    private TableColumn<WypozyczeniaProperty, String> wypozyczeniaAutorColumn;
    /**
     * Prywatne pole przechowujące referencję kolumny z ISBN książki
     */
    @FXML
    private TableColumn<WypozyczeniaProperty, String> wypozyczeniaIsbnColumn;
    /**
     * Prywatne pole przechowujące referencję kolumny z datą do której książka jest wypożyczona
     */
    @FXML
    private TableColumn<WypozyczeniaProperty, Date> wypozyczeniaWypozyczoneDoColumn;

    /**
     * Prywatne pole przechowujące referencję do pola tekstowego wyszukiwarki z id czytelnika
     */
    @FXML
    private TextField czytelnicyIdSearchBox;
    /**
     * Prywatne pole przechowujące referencję do pola tekstowego wyszukiwarki z imieniem czytelnika
     */
    @FXML
    private TextField czytelnicyImieSearchBox;
    /**
     * Prywatne pole przechowujące referencję do pola tekstowego wyszukiwarki z nazwiskiem czytelnika
     */
    @FXML
    private TextField czytelnicyNazwiskoSearchBox;
    /**
     * Prywatne pole przechowujące referencję tabeli z czytelnikami
     */
    @FXML
    private TableView<CzytelnicyProperty> czytelnicyTable;
    /**
     * Prywatne pole przechowujące referencję kolumny z id czytelnika
     */
    @FXML
    private TableColumn<CzytelnicyProperty, Long> czytelnicyIdColumn;
    /**
     * Prywatne pole przechowujące referencję kolumny z imieniem czytelnika
     */
    @FXML
    private TableColumn<CzytelnicyProperty, String> czytelnicyImieColumn;
    /**
     * Prywatne pole przechowujące referencję kolumny z nazwiskiem czytelnika
     */
    @FXML
    private TableColumn<CzytelnicyProperty, String> czytelnicyNazwiskoColumn;
    /**
     * Prywatne pole przechowujące referencję kolumny z datą założenia kary przez czytelnika
     */
    @FXML
    private TableColumn<CzytelnicyProperty, Date> czytelnicyCzlonekOdColumn;

    /**
     * metoda wywoływana przy tworzeniu okna aplikacji, wywołuje 3 metody ustawiające odpowiednio tabele
     */
    @FXML
    private void initialize() {
        prepareBooksTable();
        prepareCheckOutsTable();
        prepareReadersTable();
    }

    /**
     * Metoda ustawiająca w tabeli z książkami:
     * - CellValueFactory kolumn aby poprawnie wyświetlały dane z bazy danych,
     * - CellFactory kolumn aby poprawnie wyświetlały datę oraz zmieniały kolor komórek tabeli i tekstu w zależności od daty w komórce,
     * - RowFactory wierszy aby pod dwuklikiem na wierszu wyświetlał się dialog z dodatkowymi opcjami
     * - Dodająca limitery do pól wyszukiwania
     */
    private void prepareBooksTable() {
        ksiazkiIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        ksiazkiTytulColumn.setCellValueFactory(new PropertyValueFactory<>("tytul"));
        ksiazkiAutorColumn.setCellValueFactory(new PropertyValueFactory<>("autor"));
        ksiazkiIsbnColumn.setCellValueFactory(new PropertyValueFactory<>("isbn"));
        ksiazkiWydawnictwoColumn.setCellValueFactory(new PropertyValueFactory<>("wydawnictwo"));
        ksiazkiDataWydaniaColumn.setCellValueFactory(new PropertyValueFactory<>("dataWydania"));
        ksiazkiDataWydaniaColumn.setCellFactory(column -> new TableCell<>() {
            private final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                } else {
                    this.setText(format.format(item));
                }
            }
        });
        ksiazkiWypozyczoneDoColumn.setCellValueFactory(new PropertyValueFactory<>("wypozyczoneDo"));
        ksiazkiWypozyczoneDoColumn.setCellFactory(column -> new TableCell<>() {
            private final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                } else {
                    if (item == null) {
                        setText("Dostępna");
                        setTextFill(Color.GREEN);
                        setStyle(null);

                    } else {
                        setText(format.format(item));
                        if (item.before(new Date())) {
                            setTextFill(Color.RED);
                            setStyle("-fx-background-color: yellow");
                        }
                        else {
                            setTextFill(Color.valueOf("0x000000ff"));
                            setStyle(null);
                        }
                    }
                }
            }
        });
        TypeLimiter.addNumbersTypeLimiter(ksiazkiIdSearchBox);

        ksiazkiTable.setRowFactory(tv -> {
            TableRow<KsiazkiProperty> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    KsiazkiProperty rowData = row.getItem();
                    advancedOptionsBook(rowData);
                }
            });
            return row;
        });

        listAllBooks();
    }
    /**
     * Metoda ustawiająca w tabeli z wypożyczeniami:
     * - CellValueFactory kolumn aby poprawnie wyświetlały dane z bazy danych,
     * - CellFactory kolumn aby poprawnie wyświetlały datę oraz zmieniały kolor komórek tabeli i tekstu w zależności od daty w komórce,
     * - RowFactory wierszy aby pod dwuklikiem na wierszu wyświetlał się dialog z dodatkowymi opcjami
     * - Dodająca limitery do pól wyszukiwania
     */
    private void prepareCheckOutsTable() {
        wypozyczeniaIdKartyColumn.setCellValueFactory(new PropertyValueFactory<>("czytelnicyId"));
        wypozyczeniaIdKsiazkiColumn.setCellValueFactory(new PropertyValueFactory<>("ksiazkiId"));
        wypozyczeniaTytulColumn.setCellValueFactory(new PropertyValueFactory<>("tytul"));
        wypozyczeniaAutorColumn.setCellValueFactory(new PropertyValueFactory<>("autor"));
        wypozyczeniaIsbnColumn.setCellValueFactory(new PropertyValueFactory<>("isbn"));
        wypozyczeniaWypozyczoneDoColumn.setCellValueFactory(new PropertyValueFactory<>("wypozyczoneDo"));
        wypozyczeniaWypozyczoneDoColumn.setCellFactory(column -> new TableCell<>() {
            private final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                    setStyle(null);
                    setTextFill(null);
                } else {
                    if (item != null) {
                        this.setText(format.format(item));
                        if (item.before(new Date())) {
                            setTextFill(Color.RED);
                            setStyle("-fx-background-color: yellow");
                        }
                        else {
                            setTextFill(Color.valueOf("0x000000ff"));
                            setStyle(null);
                        }
                    }
                }
            }
        });
        TypeLimiter.addNumbersTypeLimiter(wypozyczeniaKartySearchBox);
        TypeLimiter.addNumbersTypeLimiter(wypozyczeniaKsiazkiSearchBox);

        wypozyczeniaTable.setRowFactory(tv -> {
            TableRow<WypozyczeniaProperty> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    WypozyczeniaProperty rowData = row.getItem();
                    advancedOptionsCheckOut(rowData);
                }
            });
            return row;
        });

        listAllCheckOuts();
    }

    /**
     * Metoda ustawiająca w tabeli z czytelnikami:
     * - CellValueFactory kolumn aby poprawnie wyświetlały dane z bazy danych,
     * - CellFactory kolumn aby poprawnie wyświetlały datę oraz zmieniały kolor komórek tabeli i tekstu w zależności od daty w komórce,
     * - RowFactory wierszy aby pod dwuklikiem na wierszu wyświetlał się dialog z dodatkowymi opcjami
     * - Dodająca limitery do pól wyszukiwania
     */
    private void prepareReadersTable() {
        czytelnicyIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        czytelnicyImieColumn.setCellValueFactory(new PropertyValueFactory<>("imie"));
        czytelnicyNazwiskoColumn.setCellValueFactory(new PropertyValueFactory<>("nazwisko"));
        czytelnicyCzlonekOdColumn.setCellValueFactory(new PropertyValueFactory<>("dataDolaczenia"));
        czytelnicyCzlonekOdColumn.setCellFactory(column -> new TableCell<>() {
            private final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                } else {
                    this.setText(format.format(item));
                }
            }
        });
        TypeLimiter.addNumbersTypeLimiter(czytelnicyIdSearchBox);

        czytelnicyTable.setRowFactory(tv -> {
            TableRow<CzytelnicyProperty> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    CzytelnicyProperty rowData = row.getItem();
                    advancedOptionsReader(rowData);
                }
            });
            return row;
        });

        listAllReaders();
    }

    /**
     * Metoda czyszcząca pola wyszukiwania
     */
    @FXML
    private void ksiazkiClearSearchBoxes() {
        ksiazkiIdSearchBox.clear();
        ksiazkiTytulSearchBox.clear();
        ksiazkiAutorSearchBox.clear();
        ksiazkiIsbnSearchBox.clear();
        ksiazkiWydawnictwoSearchBox.clear();

        listAllBooks();
    }

    /**
     * Metoda czyszcząca pola wyszukiwania
     */
    @FXML
    private void wypozyczeniaClearSearchBoxes() {
        wypozyczeniaKartySearchBox.clear();
        wypozyczeniaKsiazkiSearchBox.clear();

        listAllCheckOuts();
    }

    /**
     * Metoda czyszcząca pola wyszukiwania
     */
    @FXML
    private void czytelnicyClearSearchBoxes() {
        czytelnicyIdSearchBox.clear();
        czytelnicyImieSearchBox.clear();
        czytelnicyNazwiskoSearchBox.clear();

        listAllReaders();
    }

    /**
     * Metoda wypełniająca tabelę danymi wszystkich książek
     */
    private void listAllBooks() {
        ListOfKsiazki listOfKsiazki = connection.listAllBooks();
        ksiazkiTable.setItems(listOfKsiazki.getObservableList());
    }

    /**
     * Metoda wypełniająca tabelę danymi wszystkich wypożyczeń
     */
    private void listAllCheckOuts() {
        ListOfWypozyczenia listOfWypozyczenia = connection.listAllCheckOuts();
        wypozyczeniaTable.setItems(listOfWypozyczenia.getObservableList());
    }

    /**
     * Metoda wypełniająca tabelę danymi wszystkich czytelników
     */
    private void listAllReaders() {
        ListOfCzytelnicy listOfCzytelnicy = connection.listAllReaders();
        czytelnicyTable.setItems(listOfCzytelnicy.getObservableList());
    }

    /**
     * Metoda wypełniająca tabelę danymi książek z uwzględnieniem danych w polach wyszukiwania
     */
    @FXML
    private void ksiazkiSearch() {
        KsiazkiEntity entity = new KsiazkiEntity();
        if (!ksiazkiIdSearchBox.getText().equals("")) {
            entity.setId(Long.parseLong(ksiazkiIdSearchBox.getText()));
        } else {
            if (!ksiazkiTytulSearchBox.getText().equals("")) {
                entity.setTytul("%" + ksiazkiTytulSearchBox.getText() + "%");
            }
            if (!ksiazkiAutorSearchBox.getText().equals("")) {
                entity.setAutor("%" + ksiazkiAutorSearchBox.getText() + "%");
            }
            if (!ksiazkiIsbnSearchBox.getText().equals("")) {
                entity.setIsbn("%" + ksiazkiIsbnSearchBox.getText() + "%");
            }
            if (!ksiazkiWydawnictwoSearchBox.getText().equals("")) {
                entity.setWydawnictwo("%" + ksiazkiWydawnictwoSearchBox.getText() + "%");
            }
        }
        if (entity.equals(new KsiazkiEntity())) {
            listAllBooks();
        } else {
            ListOfKsiazki listOfKsiazki = connection.searchBooks(entity);
            ksiazkiTable.setItems(listOfKsiazki.getObservableList());
        }
    }

    /**
     * Metoda wypełniająca tabelę danymi wypożyczeń z uwzględnieniem danych w polach wyszukiwania
     */
    @FXML
    private void wypozyczeniaSearch() {
        WypozyczeniaEntity entity = new WypozyczeniaEntity();
        if (!wypozyczeniaKsiazkiSearchBox.getText().equals("")) {
            entity.setKsiazkiId(Long.parseLong(wypozyczeniaKsiazkiSearchBox.getText()));
        }
        if (!wypozyczeniaKartySearchBox.getText().equals("")) {
            entity.setCzytelnicyId(Long.parseLong(wypozyczeniaKartySearchBox.getText()));
        }
        if (entity.equals(new WypozyczeniaEntity())) {
            listAllCheckOuts();
        } else {
            ListOfWypozyczenia listOfWypozyczenia = connection.searchCheckOuts(entity);
            wypozyczeniaTable.setItems(listOfWypozyczenia.getObservableList());
        }
    }

    /**
     * Metoda wypełniająca tabelę danymi czytelników z uwzględnieniem danych w polach wyszukiwania
     */
    @FXML
    private void czytelnicySearch() {
        CzytelnicyEntity entity = new CzytelnicyEntity();
        if (!czytelnicyIdSearchBox.getText().equals("")) {
            entity.setId(Long.parseLong(czytelnicyIdSearchBox.getText()));
        } else {
            if (!czytelnicyImieSearchBox.getText().equals("")) {
                entity.setImie("%" + czytelnicyImieSearchBox.getText() + "%");
            }
            if (!czytelnicyNazwiskoSearchBox.getText().equals("")) {
                entity.setNazwisko("%" + czytelnicyNazwiskoSearchBox.getText() + "%");
            }
        }
        if (entity.equals(new CzytelnicyEntity())) {
            listAllReaders();
        } else {
            ListOfCzytelnicy listOfCzytelnicy = connection.searchReaders(entity);
            czytelnicyTable.setItems(listOfCzytelnicy.getObservableList());
        }
    }

    /**
     * Metoda wypełniająca tabelę danymi wypożyczeń z przetrzymanymi książkami
     */
    @FXML
    private void wypozyczeniaZalegle() {
        ListOfWypozyczenia listOfWypozyczenia = connection.searchTerminatedCheckOuts();
        wypozyczeniaTable.setItems(listOfWypozyczenia.getObservableList());
    }

    /**
     * Metoda wyświetlająca nowy dialog dodający książkę oraz w zależności od akcji użytkownika dodająca książkę
     * lub informująca o błędzie w podanych danych
     */
    @FXML
    private void addBook() {
        AddBookDialog dialog = new AddBookDialog();

        Optional<Pair<String, KsiazkiEntity>> result = dialog.showAndWait();
        if (result.isPresent()) {
            switch (result.get().getKey()) {
                case "success":
                    String id = connection.addBook(result.get().getValue());
                    if (id.equals("null")) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Błąd");
                        alert.setContentText("Nie udało się dodać nowej książki, " +
                                "spróbuj ponownie później lub skontaktuj się z pomocą techniczną.");
                        alert.showAndWait();
                    } else {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Sukces");
                        alert.setContentText("Numer nowej książki to:" + id + ".");
                        alert.showAndWait();
                        ksiazkiSearch();
                    }
                    break;
                case "blank fields": {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Puste pola");
                    alert.setContentText("Wypełnij wszystkie pola w formularzu.");
                    alert.showAndWait();
                    break;
                }
                case "bad isbn": {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Nieprawidłowy ISBN");
                    alert.setContentText("Podany ISBN jest niepoprawny.");
                    alert.showAndWait();
                    break;
                }
            }
        }
    }

    /**
     * Metoda wyświetlająca nowy dialog dodający wypożyczenie oraz w zależności od akcji użytkownika dodająca wypożyczenie
     * lub informująca o błędzie w podanych danych
     * @param property obiekt klasy KsiążkiProperty zawierający dane o książce która ma być wypożyczona
     */
    private void addCheckOut(KsiazkiProperty property) {
        AddCheckOutDialog dialog = new AddCheckOutDialog(property, connection);

        Optional<Pair<String, WypozyczeniaEntity>> result = dialog.showAndWait();
        if (result.isPresent()) {
            if (result.get().getKey().equals("add")) {
                String id = connection.addCheckOut(result.get().getValue());
                if (id.equals("null")) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Błąd");
                    alert.setContentText("Nie udało się dodać wypożyczenia, " +
                            "spróbuj ponownie później lub skontaktuj się z pomocą techniczną.");
                    alert.showAndWait();
                } else {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Sukces");
                    alert.setContentText("Pomyślnie wypożyczono książkę o numerze:" + property.getId() +
                            " czytelnikowi o numerze karty: " + id);
                    alert.showAndWait();
                    wypozyczeniaSearch();
                }
            } else if (result.get().getKey().equals("blank fields")) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Brak wyboru");
                alert.setContentText("Podaj czytelnika.");
                alert.showAndWait();
            }
        }
    }

    /**
     * Metoda wyświetlająca nowy dialog dodający czytelnika oraz w zależności od akcji użytkownika dodająca czytelnika
     * lub informująca o błędzie w podanych danych
     */
    @FXML
    private void addReader() {
        AddReaderDialog dialog = new AddReaderDialog();

        Optional<Pair<String, CzytelnicyEntity>> result = dialog.showAndWait();
        if (result.isPresent()) {
            if (result.get().getKey().equals("success")) {
                String id = connection.addReader(result.get().getValue());
                if (id.equals("null")) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Błąd");
                    alert.setContentText("Nie udało się dodać nowego czytelnika, " +
                            "spróbuj ponownie później lub skontaktuj się z pomocą techniczną.");
                    alert.showAndWait();
                } else {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Sukces");
                    alert.setContentText("Numer karty nowego czytelnika to:" + id + ".");
                    alert.showAndWait();
                    czytelnicySearch();
                }
            } else if (result.get().getKey().equals("blank fields")) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Puste pola");
                alert.setContentText("Wypełnij wszystkie pola w formularzu.");
                alert.showAndWait();
            }
        }
    }

    /**
     * Metoda wyświetlająca nowy dialog edycji książki oraz w zależności od akcji użytkownika edytująca książkę
     * lub informująca o błędzie w podanych danych
     * @param property obiekt klasy KsiążkiProperty zawierający dane o książce którą edytujemy
     */
    private void advancedOptionsBook(KsiazkiProperty property) {
        EditBookDialog dialog = new EditBookDialog(property);

        Optional<Pair<String, Object>> result = dialog.showAndWait();
        if (result.isPresent()) {
            switch (result.get().getKey()) {
                case "edit":
                    String id = connection.updateBook((KsiazkiEntity) result.get().getValue());
                    if (id.equals("null")) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Błąd");
                        alert.setContentText("Nie udało się edytować książki, " +
                                "spróbuj ponownie później lub skontaktuj się z pomocą techniczną.");
                        alert.showAndWait();
                    } else {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Sukces");
                        alert.setContentText("Pomyślnie edytowano książkę o numerze:" + id + ".");
                        alert.showAndWait();
                        ksiazkiSearch();
                    }
                    break;
                case "blank fields": {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Puste pola");
                    alert.setContentText("Wypełnij wszystkie pola w formularzu.");
                    alert.showAndWait();
                    break;
                }
                case "bad isbn": {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Nieprawidłowy ISBN");
                    alert.setContentText("Podany ISBN jest niepoprawny.");
                    alert.showAndWait();
                    break;
                }
                case "remove":
                    String response = connection.removeBook((KsiazkiEntity) result.get().getValue());
                    if (response.equals("null")) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Błąd");
                        alert.setContentText("Nie udało się usunąć książki, " +
                                "spróbuj ponownie później lub skontaktuj się z pomocą techniczną.");
                        alert.showAndWait();
                    } else {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Sukces");
                        alert.setContentText("Pomyślnie usunięto książkę o numerze: " + response + ".");
                        alert.showAndWait();
                        ksiazkiSearch();
                    }
                    break;
                case "checkOut":
                    addCheckOut((KsiazkiProperty) result.get().getValue());
            }
        }
    }

    /**
     * Metoda wyświetlająca nowy dialog kończący wypożyczenie lub przedłużający je
     * @param property obiekt klasy WypozyczeniaProperty zawierający dane o wypożyczeniu które edytujemy
     */
    private void advancedOptionsCheckOut(WypozyczeniaProperty property) {
        EditCheckOutDialog dialog = new EditCheckOutDialog(property);

        Optional<Pair<String, WypozyczeniaEntity>> result = dialog.showAndWait();
        if (result.isPresent()) {
            if (result.get().getKey().equals("prolong")) {
                String response = connection.updateCheckOut(result.get().getValue());
                if (response.equals("null")) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Błąd");
                    alert.setContentText("Nie udało się edytować czytelnika, " +
                            "spróbuj ponownie później lub skontaktuj się z pomocą techniczną.");
                    alert.showAndWait();
                } else {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Sukces");
                    alert.setContentText("Pomyślnie wypożyczono książkę o numerze:" + property.getKsiazkiId() +
                            " czytelnikowi o numerze karty: " + response);
                    alert.showAndWait();
                    wypozyczeniaSearch();
                }
            } else if (result.get().getKey().equals("remove")) {
                String response = connection.removeCheckOut(result.get().getValue());
                if (response.equals("null")) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Błąd");
                    alert.setContentText("Nie udało się usunąć wypożyczenia, " +
                            "spróbuj ponownie później lub skontaktuj się z pomocą techniczną.");
                    alert.showAndWait();
                } else {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Sukces");
                    alert.setContentText("Pomyślnie usunięto wypożyczenie książki o numerze:" + property.getKsiazkiId() +
                            " przez czytelnika o numerze karty: " + response);
                    alert.showAndWait();
                    wypozyczeniaSearch();
                }
            }
        }
    }

    /**
     * Metoda wyświetlająca nowy dialog edycji czytelnika oraz w zależności od akcji użytkownika edytująca czytelnika
     * lub informująca o błędzie w podanych danych
     * @param property obiekt klasy CzytelnicyProperty zawierający dane o czytelniku którego edytujemy
     */
    private void advancedOptionsReader(CzytelnicyProperty property) {
        EditReaderDialog dialog = new EditReaderDialog(property);

        Optional<Pair<String, CzytelnicyEntity>> result = dialog.showAndWait();
        if (result.isPresent()) {
            switch (result.get().getKey()) {
                case "edit": {
                    String response = connection.updateReader(result.get().getValue());
                    if (response.equals("null")) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Błąd");
                        alert.setContentText("Nie udało się edytować czytelnika, " +
                                "spróbuj ponownie później lub skontaktuj się z pomocą techniczną.");
                        alert.showAndWait();
                    } else {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Sukces");
                        alert.setContentText("Pomyślnie edytowano czytelnika o numerze karty " + response + ".");
                        alert.showAndWait();
                        czytelnicySearch();
                    }
                    break;
                }
                case "remove": {
                    String response = connection.removeReader(result.get().getValue());
                    if (response.equals("null")) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Błąd");
                        alert.setContentText("Nie udało się usunąć czytelnika, " +
                                "spróbuj ponownie później lub skontaktuj się z pomocą techniczną.");
                        alert.showAndWait();
                    } else {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Sukces");
                        alert.setContentText("Pomyślnie usunięto czytelnika o numerze karty " + response + ".");
                        alert.showAndWait();
                        czytelnicySearch();
                    }
                    break;
                }
                case "blank fields":
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Puste pola");
                    alert.setContentText("Wypełnij wszystkie pola w formularzu.");
                    alert.showAndWait();
                    break;
            }
        }
    }

    /**
     * Metoda zwracająca połączenie z sewerem
     * @return połączenie z serwerem
     */
    ConnectionToServer getConnection() {
        return connection;
    }
}


