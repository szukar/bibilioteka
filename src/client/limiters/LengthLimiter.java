package client.limiters;

import javafx.scene.control.TextField;

/**
 * klasa zawierająca metodę statyczną dodającą limit znaków to pola tekstowego
 */
public class LengthLimiter {
    /**
     * Ustawia limit znaków dla pola tekstowego
     * @param field pole do limitowania
     * @param maxLength maksymalna ilość znaków
     */
    public static void addLengthLimiter(TextField field, int maxLength) {
        field.textProperty().addListener((ov, oldValue, newValue) -> {
            if (field.getText().length() > maxLength) {
                String s = field.getText().substring(0, maxLength);
                field.setText(s);
            }
        });
    }
}
