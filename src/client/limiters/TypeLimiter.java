package client.limiters;

import javafx.scene.control.TextField;

/**
 * klasa zawierająca metodę statyczną dodającą limit znaków to pola tekstowego
 */
public class TypeLimiter {
    /**
     * Metoda dodająca limit pozwalający wpisywać w pole tekstowe tylko cyfry
     * @param field pole tekstowe do którego zostanie dodane ograniczenie
     */
    public static void addNumbersTypeLimiter(TextField field) {
        field.textProperty().addListener((ov, oldValue, newValue) -> {
            if (!field.getText().matches("[0-9]+")) {
                field.setText(oldValue);
            }
        });
    }
}
