package client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Główne okno aplikacji
 * @author Szymon Żebrowski
 * @version 1.0
 */
public class Library extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        System.out.println("[Client]: Łączę się");
        System.out.println("[Client]: Połączono");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Library.fxml"));
        Scene scene = new Scene(loader.load());
        LibraryController controller = loader.getController();
        ConnectionToServer connection = controller.getConnection();
        primaryStage.setScene(scene);
        primaryStage.setTitle("System obsługi biblioteki");
        primaryStage.setOnCloseRequest(t -> {
            try {
                connection.close();
            } catch (IOException e) {
                e.printStackTrace();
                Platform.exit();
                System.exit(-1);
            }
            Platform.exit();
            System.exit(0);
        });
        primaryStage.show();
    }
}

