package client;

import containers.entities.CzytelnicyEntity;
import containers.entities.KsiazkiEntity;
import containers.entities.WypozyczeniaEntity;
import containers.lists.ListOfCzytelnicy;
import containers.lists.ListOfKsiazki;
import containers.lists.ListOfWypozyczenia;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Klasa reprezentująca połączenie z serwerem
 * @author Szymon Żebrowski
 * @version 1.0
 */
public class ConnectionToServer {
    /**
     * Prywatne pole przechowujące kolejkę wiadomości z serwera
     */
    private final Queue<String> messages = new LinkedList<>();

    /**
     * Prywatne pole przechowujące output stream do serwera
     */
    private ObjectOutputStream outputStream;

    /**
     * Konstruktor łączący się z serwerem oraz ustawiający podtrzymywanie połączenia
     */
    ConnectionToServer() {
        try {
            Socket socket = new Socket("localhost", 7778);
            socket.setKeepAlive(true);
            socket.setSoTimeout(200);
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
            ClientReceiver receiver = new ClientReceiver(messages, inputStream);
            receiver.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda zamykająca połączenie z serwerem
     */
    void close() throws IOException {
        outputStream.writeObject("close");
    }

    /**
     * Metoda czekająca na wiadomości od serwera
     */
    private void waitForMassages () {
        while (messages.size() == 0) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    

    /**
     * Metoda zwracająca listę wszystkich książek
     * @return lista książek
     */
    ListOfKsiazki listAllBooks() {
        try {
            String request = "request listAllBooks";
            outputStream.writeObject(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return ListOfKsiazki.toObject(messages.poll());
    }

    /**
     * Metoda zwracająca listę wszystkich czytelników
     * @return lista czytelników
     */
    public ListOfCzytelnicy listAllReaders() {
        try {
            String request = "request listAllReaders";
            outputStream.writeObject(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
        waitForMassages();
        return ListOfCzytelnicy.toObject(messages.poll());
    }

    /**
     * Metoda zwracająca listę wszystkich wypożyczeń
     * @return lista wypożyczeń
     */
    ListOfWypozyczenia listAllCheckOuts() {
        try {
            String request = "request listAllCheckOuts";
            outputStream.writeObject(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return ListOfWypozyczenia.toObject(messages.poll());
    }

    /**
     * Metoda zwracająca listę książek o właściwościach dostarczonej encji
     * @param entity encja z informacjami o właściwościach poszukiwanych książek
     * @return lista wyników
     */
    ListOfKsiazki searchBooks(KsiazkiEntity entity) {
        try {
            String request = "request searchBooks";
            outputStream.writeObject(request);
            outputStream.writeObject(KsiazkiEntity.toXML(entity));
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return ListOfKsiazki.toObject(messages.poll());
    }

    /**
     * Metoda zwracająca listę wypożyczeń o właściwościach dostarczonej encji
     * @param entity encja z informacjami o właściwościach poszukiwanych wypożyczeń
     * @return lista wyników
     */
    ListOfWypozyczenia searchCheckOuts(WypozyczeniaEntity entity) {
        try {
            String request = "request searchCheckOuts";
            outputStream.writeObject(request);
            outputStream.writeObject(WypozyczeniaEntity.toXML(entity));
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return ListOfWypozyczenia.toObject(messages.poll());
    }

    /**
     * Metoda zwracająca listę wypożyczeń przetrzymanych książek
     * @return lista wyników
     */
    ListOfWypozyczenia searchTerminatedCheckOuts() {
        try {
            String request = "request searchTerminatedCheckOuts";
            outputStream.writeObject(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return ListOfWypozyczenia.toObject(messages.poll());
    }

    /**
     * Metoda zwracająca listę czytelników o właściwościach dostarczonej encji
     * @param entity encja z informacjami o właściwościach poszukiwanych czytelników
     * @return lista wyników
     */
    public ListOfCzytelnicy searchReaders(CzytelnicyEntity entity) {
        try {
            String request = "request searchReaders";
            outputStream.writeObject(request);
            outputStream.writeObject(CzytelnicyEntity.toXML(entity));
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return ListOfCzytelnicy.toObject(messages.poll());
    }

    /**
     * metoda dodająca podaną encję do bazy
     * @param entity encja do dodania
     * @return id encji w bazie
     */
    String addBook(KsiazkiEntity entity) {
        try {
            String request = "request addBook";
            outputStream.writeObject(request);
            outputStream.writeObject(KsiazkiEntity.toXML(entity));
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return messages.poll();
    }

    /**
     * metoda dodająca podaną encję do bazy
     * @param entity encja do dodania
     * @return id encji w bazie
     */
    String addCheckOut(WypozyczeniaEntity entity) {
        try {
            String request = "request addCheckOut";
            outputStream.writeObject(request);
            outputStream.writeObject(WypozyczeniaEntity.toXML(entity));
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return messages.poll();
    }

    /**
     * metoda dodająca podaną encję do bazy
     * @param entity encja do dodania
     * @return id encji w bazie
     */
    String addReader(CzytelnicyEntity entity) {
        try {
            String request = "request addReader";
            outputStream.writeObject(request);
            outputStream.writeObject(CzytelnicyEntity.toXML(entity));
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return messages.poll();
    }

    /**
     * metoda zmieniająca podaną encję w bazie
     * @param entity encja do zmienienia
     * @return id encji w bazie
     */
    String updateBook(KsiazkiEntity entity) {
        try {
            String request = "request updateBook";
            outputStream.writeObject(request);
            outputStream.writeObject(KsiazkiEntity.toXML(entity));
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return messages.poll();
    }

    /**
     * metoda zmieniająca podaną encję w bazie
     * @param entity encja do zmienienia
     * @return id encji w bazie
     */
    String updateCheckOut(WypozyczeniaEntity entity) {
        try {
            String request = "request updateCheckOut";
            outputStream.writeObject(request);
            outputStream.writeObject(WypozyczeniaEntity.toXML(entity));
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return messages.poll();
    }

    /**
     * metoda zmieniająca podaną encję w bazie
     * @param entity encja do zmienienia
     * @return id encji w bazie
     */
    String updateReader(CzytelnicyEntity entity) {
        try {
            String request = "request updateReader";
            outputStream.writeObject(request);
            outputStream.writeObject(CzytelnicyEntity.toXML(entity));
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return messages.poll();
    }

    /**
     * metoda usuwająca podaną encję z bazy
     * @param entity encja do usunięcia
     * @return id encji w bazie
     */
    String removeBook(KsiazkiEntity entity) {
        try {
            String request = "request removeBook";
            outputStream.writeObject(request);
            outputStream.writeObject(KsiazkiEntity.toXML(entity));
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return messages.poll();
    }

    /**
     * metoda usuwająca podaną encję z bazy
     * @param entity encja do usunięcia
     * @return id encji w bazie
     */
    String removeCheckOut(WypozyczeniaEntity entity) {
        try {
            String request = "request removeCheckOut";
            outputStream.writeObject(request);
            outputStream.writeObject(WypozyczeniaEntity.toXML(entity));
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return messages.poll();
    }

    /**
     * metoda usuwająca podaną encję z bazy
     * @param entity encja do usunięcia
     * @return id encji w bazie
     */
    String removeReader(CzytelnicyEntity entity) {
        try {
            String request = "request removeReader";
            outputStream.writeObject(request);
            outputStream.writeObject(CzytelnicyEntity.toXML(entity));
        } catch (IOException e) {
            e.printStackTrace();
        }
        waitForMassages();
        return messages.poll();
    }
}
