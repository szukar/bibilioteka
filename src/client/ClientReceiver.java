package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.SocketTimeoutException;
import java.util.Queue;

/**
 * Klasa odbierająca informacje od serwera i dodająca je do kolejki wiadomości
 * @author Szymon Żebrowski
 * @version 1.0
 */
class ClientReceiver extends Thread {
    /**
     * Prywatne pole przechowujące kolejkę wiadomości
     */
    private final Queue<String> messages;

    /**
     * Prywatne pole przechowujące input stream od serwera
     */
    private final ObjectInputStream inputStream;

    /**
     * Prywatne pole przechowujące informację czy dalej odpierać informacje
     */
    private Boolean interrupted = false;

    /**
     * Konstruktor
     * @param m kolejka do której dodawane są wiadomości
     * @param is input stream od serwera
     */
    ClientReceiver(Queue<String> m, ObjectInputStream is) {
        messages = m;
        inputStream = is;
    }

    /**
     * Metoda przerywająca działanie pętli
     */
    public void interrupt() {
        interrupted = true;
    }

    /**
     * Metoda zawierająca pętlę w której są odbierane i dodawane do kolejki wiadomości od serwera
     */
    @Override
    public void run() {
        long lastRead = 0;
        while (!interrupted) {
            try {
                String data = (String) inputStream.readObject();
                if (data == null) continue;
                lastRead = System.currentTimeMillis();
                if (data.equals("MSG_HEARTBEAT")) continue;
                messages.add(data);
                System.out.println("\treceived data:");
                System.out.println(data);
            } catch (SocketTimeoutException e) {
                if (10 * 1000 < System.currentTimeMillis() - lastRead) {
                    System.out.println("connection reset");
                    interrupt();
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
