package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Queue;

class ServerReceiver implements Runnable {
    private final Queue<String> messages;
    private final ObjectInputStream inputStream;
    private Boolean interrupted = false;

    private void interrupt() {
        interrupted = true;
    }

    ServerReceiver(Queue<String> m, ObjectInputStream is) {
        messages = m;
        inputStream = is;
    }

    @Override
    public void run() {
        while (!interrupted) {
            try {
                String data = (String) inputStream.readObject();
                messages.add(data);
                if (data.equals("close"))
                    interrupt();
                System.out.println("\treceived data:");
                System.out.println(data);
            } catch (SocketTimeoutException e) {
                //
            } catch (SocketException e) {
                e.printStackTrace();
                interrupt();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
