package server;

import java.io.IOException;
import java.io.ObjectOutputStream;

class HeartbeatSender implements Runnable {
    private final ObjectOutputStream outputStream;
    private Boolean interrupted = false;

    public void interrupt() {
        interrupted = true;
    }

    public HeartbeatSender(ObjectOutputStream outputStream) {
        this.outputStream = outputStream;
    }

    @Override
    public void run() {
        while (!interrupted) {
            try {
                outputStream.writeObject("MSG_HEARTBEAT");
                Thread.sleep(5*1000);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
