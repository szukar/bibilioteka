package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

class Server {
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(7778);
            //System.out.println("[Server]: Nasłuchuję");
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("[Server]: Nowe połączenie");
                ServerService service = new ServerService(socket);
                service.run();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
