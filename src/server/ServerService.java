package server;

import containers.entities.CzytelnicyEntity;
import containers.entities.KsiazkiEntity;
import containers.entities.WypozyczeniaEntity;
import containers.lists.ListOfCzytelnicy;
import containers.lists.ListOfKsiazki;
import containers.lists.ListOfWypozyczenia;
import org.hibernate.*;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Restrictions;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.*;

class ServerService implements Runnable {
    private final Socket socket;
    private ObjectOutputStream outputStream;
    private SessionFactory factory;
    private final Queue<String> messages = new LinkedList<>();
    private Boolean interrupted = false;
    private HeartbeatSender sender;

    ServerService(Socket s) {
        socket = s;
    }

    private void interrupt() {
        interrupted = true;
    }

    private void init() throws IOException {
        //System.out.println("[Service]: Początek init");
        socket.setKeepAlive(true);
        socket.setSoTimeout(200);

        outputStream = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
        ServerReceiver receiver = new ServerReceiver(messages, inputStream);
        Thread receiverThread = new Thread(receiver);
        receiverThread.start();
        sender = new HeartbeatSender(outputStream);
        Thread senderThread = new Thread(sender);
        senderThread.start();

        StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
        Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
        factory = metadata.getSessionFactoryBuilder().build();
        //System.out.println("[Service]: Koniec init");
    }

    private void waitForMassages () {
        while (messages.size() == 0) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void requestHandling(String request) {
        if (request.contains("request")) {
            String[] spittedRequest = request.split(" ");
            switch (spittedRequest[1]) {
                case "listAllBooks":
                    listAllBooks();
                    break;

                case "listAllCheckOuts":
                    listAllCheckOuts();
                    break;

                case "listAllReaders":
                    listAllReaders();
                    break;

                case "searchBooks":
                    searchBooks();
                    break;

                case "searchCheckOuts":
                    searchCheckOuts();
                    break;

                case "searchTerminatedCheckOuts":
                    searchTerminatedCheckOuts();
                    break;

                case "searchReaders":
                    searchReaders();
                    break;

                case "addBook":
                    addBook();
                    break;

                case "addCheckOut":
                    addCheckOut();
                    break;

                case "addReader":
                    addReader();
                    break;

                case "updateBook":
                    updateBook();
                    break;

                case "updateCheckOut":
                    updateCheckOut();
                    break;

                case "updateReader":
                    updateReader();
                    break;

                case "removeBook":
                    removeBook();
                    break;

                case "removeCheckOut":
                    removeCheckOut();
                    break;

                case "removeReader":
                    removeReader();
                    break;

                default:
                    break;
            }
        }
        else {
            System.err.println("Bad request:");
            System.err.println(request);
        }

    }

    private void listAllBooks() {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        List k = null;
        List w = null;
        try {
            k = session.createQuery("FROM KsiazkiEntity").list();
            w = session.createQuery("FROM WypozyczeniaEntity").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            ListOfKsiazki listOfKsiazki = new ListOfKsiazki(k, w);
            outputStream.writeObject(ListOfKsiazki.toXML(listOfKsiazki));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void listAllCheckOuts() {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        List w = null;
        List k = new ArrayList();
        List c = new ArrayList();
        try {
            w = session.createQuery("FROM WypozyczeniaEntity").list();
            Criteria criteria;
            for (Object o : w) {
                WypozyczeniaEntity wypozyczeniaEntity = (WypozyczeniaEntity) o;
                criteria = session.createCriteria(KsiazkiEntity.class);
                criteria.add(Restrictions.eq("id", wypozyczeniaEntity.getKsiazkiId()));
                List list = criteria.list();
                k.add(list.get(0));
            }
            for (Object o : w) {
                WypozyczeniaEntity wypozyczeniaEntity = (WypozyczeniaEntity) o;
                criteria = session.createCriteria(CzytelnicyEntity.class);
                criteria.add(Restrictions.eq("id", wypozyczeniaEntity.getCzytelnicyId()));
                List list = criteria.list();
                c.add(list.get(0));
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            ListOfWypozyczenia listOfWypozyczenia = new ListOfWypozyczenia(w, k, c);
            outputStream.writeObject(ListOfWypozyczenia.toXML(listOfWypozyczenia));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void listAllReaders() {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        List list = null;
        try {
            list = session.createQuery("FROM CzytelnicyEntity").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            ListOfCzytelnicy listOfCzytelnicy = new ListOfCzytelnicy(list);
            outputStream.writeObject(ListOfCzytelnicy.toXML(listOfCzytelnicy));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void searchBooks() {
        waitForMassages();
        KsiazkiEntity entity = KsiazkiEntity.toObject(messages.poll());
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        Criteria criteria = null;
        List k = null;
        List w = new ArrayList();
        try {
            if (entity.getId() == null) {
                Example example = Example.create(entity);
                example.enableLike().ignoreCase();
                criteria = session.createCriteria(KsiazkiEntity.class).add(example);
            } else {
                criteria = session.createCriteria(KsiazkiEntity.class)
                        .add(Restrictions.eq("id", entity.getId()));
            }
            k = criteria.list();
            for (Object o : k) {
                KsiazkiEntity ksiazkiEntity = (KsiazkiEntity) o;
                criteria = session.createCriteria(WypozyczeniaEntity.class);
                criteria.add(Restrictions.eq("ksiazkiId", ksiazkiEntity.getId()));
                List list = criteria.list();
                if (list != null && list.size() > 0) {
                    w.add(list.get(0));
                }
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

        try {
            ListOfKsiazki listOfKsiazki = new ListOfKsiazki(k, w);
            outputStream.writeObject(ListOfKsiazki.toXML(listOfKsiazki));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void searchCheckOuts() {
        waitForMassages();
        WypozyczeniaEntity entity = WypozyczeniaEntity.toObject(messages.poll());
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        Criteria criteria = session.createCriteria(WypozyczeniaEntity.class);
        List w = null;
        List k = new ArrayList();
        List c = new ArrayList();
        try {
            if (entity.getKsiazkiId() != null) {
                criteria.add(Restrictions.eq("ksiazkiId", entity.getKsiazkiId()));
            }
            if (entity.getCzytelnicyId() != null) {
                criteria.add(Restrictions.eq("czytelnicyId", entity.getCzytelnicyId()));
            }
            w = criteria.list();
            for (Object o : w) {
                WypozyczeniaEntity wypozyczeniaEntity = (WypozyczeniaEntity) o;
                criteria = session.createCriteria(KsiazkiEntity.class);
                criteria.add(Restrictions.eq("id", wypozyczeniaEntity.getKsiazkiId()));
                List list = criteria.list();
                k.add(list.get(0));
            }
            for (Object o : w) {
                WypozyczeniaEntity wypozyczeniaEntity = (WypozyczeniaEntity) o;
                criteria = session.createCriteria(CzytelnicyEntity.class);
                criteria.add(Restrictions.eq("id", wypozyczeniaEntity.getCzytelnicyId()));
                List list = criteria.list();
                c.add(list.get(0));
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            ListOfWypozyczenia listOfWypozyczenia = new ListOfWypozyczenia(w, k, c);
            outputStream.writeObject(ListOfWypozyczenia.toXML(listOfWypozyczenia));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void searchTerminatedCheckOuts() {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        Criteria criteria = session.createCriteria(WypozyczeniaEntity.class);
        List w = null;
        List k = new ArrayList();
        List c = new ArrayList();
        try {
            criteria.add(Restrictions.lt("wypozyczoneDo", new Date()));
            w = criteria.list();
            criteria = session.createCriteria(KsiazkiEntity.class);
            for (Object o : w) {
                WypozyczeniaEntity wypozyczeniaEntity = (WypozyczeniaEntity) o;
                criteria = session.createCriteria(KsiazkiEntity.class);
                criteria.add(Restrictions.eq("id", wypozyczeniaEntity.getKsiazkiId()));
                List list = criteria.list();
                k.add(list.get(0));
            }
            for (Object o : w) {
                WypozyczeniaEntity wypozyczeniaEntity = (WypozyczeniaEntity) o;
                criteria = session.createCriteria(CzytelnicyEntity.class);
                criteria.add(Restrictions.eq("id", wypozyczeniaEntity.getCzytelnicyId()));
                List list = criteria.list();
                c.add(list.get(0));
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            ListOfWypozyczenia listOfWypozyczenia = new ListOfWypozyczenia(w, k, c);
            outputStream.writeObject(ListOfWypozyczenia.toXML(listOfWypozyczenia));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void searchReaders() {
        waitForMassages();
        CzytelnicyEntity entity = CzytelnicyEntity.toObject(messages.poll());
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        Criteria criteria = null;
        List list = null;
        try {
            if (entity.getId() == null) {
                Example example = Example.create(entity).enableLike().ignoreCase();
                criteria = session.createCriteria(CzytelnicyEntity.class).add(example);
            } else {
                criteria = session.createCriteria(CzytelnicyEntity.class)
                        .add(Restrictions.eq("id", entity.getId()));
            }
            list = criteria.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            ListOfCzytelnicy listOfCzytelnicy = new ListOfCzytelnicy(list);
            outputStream.writeObject(ListOfCzytelnicy.toXML(listOfCzytelnicy));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addBook() {
        waitForMassages();
        KsiazkiEntity entity = KsiazkiEntity.toObject(messages.poll());
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        Long id = null;
        try {
            id = (Long) session.save(entity);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            if (id != null) {
                outputStream.writeObject(id.toString());
            } else {
                outputStream.writeObject("null");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addCheckOut() {
        waitForMassages();
        WypozyczeniaEntity entity = WypozyczeniaEntity.toObject(messages.poll());
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        Long id = null;
        try {
            id = (Long) session.save(entity);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            if (id != null) {
                outputStream.writeObject(entity.getCzytelnicyId().toString());
            } else {
                outputStream.writeObject("null");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addReader() {
        waitForMassages();
        CzytelnicyEntity entity = CzytelnicyEntity.toObject(messages.poll());
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        Long id = null;
        try {
            id = (Long) session.save(entity);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            if (id != null) {
                outputStream.writeObject(id.toString());
            } else {
                outputStream.writeObject("null");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateBook() {
        waitForMassages();
        KsiazkiEntity entity = KsiazkiEntity.toObject(messages.poll());
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(entity);
            transaction.commit();
        } catch (HibernateException e) {
            try {
                outputStream.writeObject("null");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (transaction != null) transaction.rollback();

            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            outputStream.writeObject(entity.getId().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateCheckOut() {
        waitForMassages();
        WypozyczeniaEntity entity = WypozyczeniaEntity.toObject(messages.poll());
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(entity);
            transaction.commit();
        } catch (HibernateException e) {
            try {
                outputStream.writeObject("null");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (transaction != null) transaction.rollback();

            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            outputStream.writeObject(entity.getCzytelnicyId().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateReader() {
        waitForMassages();
        CzytelnicyEntity entity = CzytelnicyEntity.toObject(messages.poll());
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(entity);
            transaction.commit();
        } catch (HibernateException e) {
            try {
                outputStream.writeObject("null");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (transaction != null) transaction.rollback();

            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            outputStream.writeObject(entity.getId().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void removeBook() {
        waitForMassages();
        KsiazkiEntity entity = KsiazkiEntity.toObject(messages.poll());
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.remove(entity);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            try {
                outputStream.writeObject("null");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            outputStream.writeObject(entity.getId().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void removeCheckOut() {
        waitForMassages();
        WypozyczeniaEntity entity = WypozyczeniaEntity.toObject(messages.poll());
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.remove(entity);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            try {
                outputStream.writeObject("null");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            outputStream.writeObject(entity.getCzytelnicyId().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void removeReader() {
        waitForMassages();
        CzytelnicyEntity entity = CzytelnicyEntity.toObject(messages.poll());
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.remove(entity);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            try {
                outputStream.writeObject("null");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        try {
            outputStream.writeObject(entity.getId().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            init();
            while (!interrupted) {
                waitForMassages();
                String request = messages.poll();
                if ("close".equals(request)) {
                    sender.interrupt();
                    socket.close();
                    interrupt();
                }
                else {
                    assert request != null;
                    requestHandling(request);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
